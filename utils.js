const Guilded = require('gdd.js');
const fs = require('fs');

const utils = {
	/**
	 * Logs all the executed commands by a user to the console
	 * 
	 * @param {Guilded.ChatMessage} message 
	 */
	logCommandExec: (message) => {
		console.log(`User: ${message.author.id}\nCommand: ${message.content}\n------------------------------------------------------------------------------------`);
	},
	/**
	 * Returns the specified string in the specified language if it is set
	 * 
	 * @param {String} lang 
	 * @param {String} strCode 
	 * 
	 * @returns {String} 
	 */
	getLocalizedString: (lang, strCode) => {
		let ret = '';
		let langConfig = JSON.parse(fs.readFileSync(`./locale/${lang}.json`, 'utf8'));
		if(typeof langConfig[strCode] === 'string') {
			ret = langConfig[strCode];
		}else {
			ret = 'ERROR pls report :gilshrug:';
		}
		return ret;
	},
	/**
	 * 
	 * @param {Guilded.ChatMessage} message
	 * @param {String} command
	 * @param {guildSettings} guildSettings
	 */
	sendUsageInstructions: (message, command, guildSettings) => {
		let helpMsg = `**${command.toUpperCase()}**\n\`\`\``;
		helpMsg += `${utils.getLocalizedString(guildSettings.lang, 'Usage')}: ${utils.getLocalizedString(guildSettings.lang, `${command}Usage`).replace('{{CMDPREFIX}}', guildSettings.cmdPrefix)}\n`;
		helpMsg += `${utils.getLocalizedString(guildSettings.lang, 'Description')}: ${utils.getLocalizedString(guildSettings.lang, `${command}Description`)}`;
		helpMsg += '```';

		message.channel.send(helpMsg);
	},
	/**
	 * Handles error codes and returns info messages to the user
	 * 
	 * @param {Guilded.ChatMessage} message 
	 * @param {String} error 
	 * @param {guildSettings} guildSettings 
	 */
	errorHandler: (message, error, guildSettings) => {
		let errorMsg = `**${utils.getLocalizedString(guildSettings.lang, 'Error')}**\n\`\`\``;
		errorMsg += `${utils.getLocalizedString(guildSettings.lang, error).replace('{{CMDPREFIX}}', guildSettings.cmdPrefix)}`;
		errorMsg += '```';

		message.channel.send(errorMsg).then(() => {
			utils.logCommandExec(message);
		});
	},
	/**
	 * Returns the server settings, if a settings file doesn't exist for that server it defaults the config/config.json, if a server file exists but isn't complete, it auto completes the missing settings from the config/config.json
	 * 
	 * @param {Guilded.ChatMessage} message 
	 * 
	 * @returns {guildSettings} 
	 */
	getGuildSettings: (message) => {
		let ret = new Object();
		let config = JSON.parse(fs.readFileSync('./configs/config.json', 'utf8'));
		if(typeof config.settings === 'object' && Object.entries(config.settings).length > 0) {
			ret = config.settings;
		}
		return ret;
	},
	/**
	 * Returns a boolean indicating if the user has permission
	 * 
	 * Examples:
	 * 
	 * 	res = checkHasPermission("command.help", 1, null, message);
	 * 	res = checkHasPermission("command.help", 0, "198830706657460224", message);
	 * 
	 * @param {String} permissionKey 
	 * @param {Number} checkOn 0 = specific server config file, 1 = general config file -- doesn't do anything for now
	 * @param {String} guildId server ID or null -- doesn't do anything for now
	 * @param {Guilded.ChatMessage} message
	 * 
	 * @returns {Boolean}
	 */
	checkHasPermission: (permissionKey, checkOn, guildId, message) => {
		if(permissionKey !== '' && permissionKey !== null) {
			if(fs.existsSync('./configs/config.json')) {
				let generalSettings = JSON.parse(fs.readFileSync('./configs/config.json', 'utf8'));
				if(Object.prototype.hasOwnProperty.call(generalSettings.permissions, permissionKey)) {
					if(Object.prototype.hasOwnProperty.call(generalSettings.permissions[permissionKey], 'users')) {
						if(generalSettings.permissions[permissionKey].users.includes('*') || generalSettings.permissions[permissionKey].users.includes(message.author.id)) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
};

module.exports = utils;

/**
 * Represents the structure of the guilded server settings object
 * @typedef guildSettings
 * @type {Object}
 * @property {String} lang - The language id (default defined on ./configs/config.json)
 * @property {String} cmdPrefix - The command prefix (default defined on ./configs/config.json)
 */