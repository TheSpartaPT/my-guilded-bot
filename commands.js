const Guilded = require('gdd.js');
const util = require('./utils.js');

/**
 * Object that contains settings for each command that exists
 * @type {Object.<string, commandSettings>}
 */
const commands = {
	'ping': {
		permissionKey: 'command.ping',
		exec: (guildSettings, message, cmdParams, client) => {
			message.channel.send(`Pong! ${Math.round(client.ping)}ms`).then(() => {
				util.logCommandExec(message);
			});
		}
	},
	'restart': {
		permissionKey: 'command.restart',
		exec: (guildSettings, message, cmdParams, client) => {
			message.channel.send('Restarting :wave:').then(() => {
				util.logCommandExec(message);
				process.exit(1);
			});
		}
	},
	'help': {
		permissionKey: 'command.help',
		exec: (guildSettings, message, cmdParams, client) => {
			if(cmdParams && cmdParams.length == 1) {
				cmdParams[0] = cmdParams[0].toLowerCase();
				if(Object.prototype.hasOwnProperty.call(commands, cmdParams[0])) {
					let helpMsg = `**${cmdParams[0].toUpperCase()}**\n\`\`\``;
					helpMsg += `${util.getLocalizedString(guildSettings.lang, 'Usage')}: ${util.getLocalizedString(guildSettings.lang, `${cmdParams[0]}Usage`).replace('{{CMDPREFIX}}', guildSettings.cmdPrefix)}\n`;
					helpMsg += `${util.getLocalizedString(guildSettings.lang, 'Description')}: ${util.getLocalizedString(guildSettings.lang, `${cmdParams[0]}Description`)}`;
					helpMsg += '```';
					
					message.channel.send(helpMsg).then(() => {
						util.logCommandExec(message);
					});
				}else {
					util.errorHandler(message, 'errorDoesntExist', guildSettings);
				}
			}else {
				let helpMsg = `**${util.getLocalizedString(guildSettings.lang, 'Commands')}**\n\`\`\``;
				let cmdIndex = 0;
				Object.keys(commands).filter((command) => {
					if(util.checkHasPermission(commands[command].permissionKey, 1, null, message)) {
						return command;
					}
				}).forEach((command) => {
					if(cmdIndex > 0) {
						helpMsg += '\n';
					}

					helpMsg += util.getLocalizedString(guildSettings.lang, `${command}Usage`).replace('{{CMDPREFIX}}', guildSettings.cmdPrefix);
					cmdIndex++;
				});
				helpMsg += '```';
				message.channel.send(helpMsg).then(() => {
					util.logCommandExec(message);
				});
			}
		}
	}
};

module.exports = commands;

/**
 * Represents the structure of a sigle command inside the object "commands"
 * @typedef commandSettings
 * @type {Object}
 * @property {String} permissionKey - A unique string that is used to identify if a person has permission for a specific command
 * @property {String} alias - Defines if a command is an alias to another command, contains a string with the name of the root command, no need to define exec if you define this
 * @property {(guildSettings: util.guildSettings, message: Guilded.ChatMessage, cmdParams: Array<string>, client: Guilded.Client) => void} exec - A method that has the code for a specific command, only used in the root command
 */
