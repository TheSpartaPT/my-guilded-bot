require('dotenv').config();

const chalk = require('chalk');
const Guilded = require('gdd.js');
const util = require('./utils.js');
const commands = require('./commands.js');

const client = new Guilded.Client();

client.on('ready', () => {
	// resets voice to normal, in case the bot is restarted while playing music (while inside a voice channel)
	/*client.guilds.cache.forEach((guild) => {
		if(guild.voice) {
			guild.voice.channel.join();
			guild.voice.channel.leave();
		}
	});*/

	console.log('------------------------------------------------------------------------------------');
	console.log(chalk.green('Ready!'));
	console.log('------------------------------------------------------------------------------------');
});

client.on('chatMessageCreated', async (message) => {
	if(typeof message.createdByBotId === 'undefined' && typeof message.createdByWebhookId === 'undefined') { //Doesn't do anything if message comes from a bot or a webhook
		let guildSettings = util.getGuildSettings(message);

		if(message.content.startsWith(guildSettings.cmdPrefix)) {
			let cmdName = message.content.split(/\s(.+)/)[0].substring(guildSettings.cmdPrefix.length);
			let cmdParams = (typeof message.content.split(/\s(.+)/)[1] !== 'undefined' ? message.content.split(/\s(.+)/)[1].split(' ') : null);
			if(commands[cmdName]) {
				if(commands[cmdName].alias) {
					cmdName = commands[cmdName].alias;
				}
				if(util.checkHasPermission(commands[cmdName].permissionKey, 1, null, message)) {
					commands[cmdName].exec(guildSettings, message, cmdParams, client);
				}else {
					util.errorHandler(message, 'errorNoPermissions', guildSettings);
				}
			}
		}
	}
});

client.connect(process.env.AUTH_TOKEN);